class api {
  async getStatProfile(username, platform) {
    const response = await fetch(`https://api.r6stats.com/api/v1/players/${username}/?platform=${platform}`);
    const json = await response.json();
    return json;
  }
  async getStatOp(username, platform) {
    const response = await fetch(`https://api.r6stats.com/api/v1/players/${username}/operators/?platform=${platform}`);
    const json = await response.json();
    return json;
  }
}

export default new api();

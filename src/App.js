import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import api from './api.js'
import ReactBodymovin from 'react-bodymovin'
import animation from './snap_loader_white.json'

class App extends Component {
  state = {
    profile: null,
    operators: null,
    username: "",
    platform: "ps4"
  }

  async componentDidMount() {
    // var t0 = performance.now();
    // const statP = await api.getStatProfile('Diiixoon', 'ps4');
    // const statO = await api.getStatOp('Diiixoon', 'ps4');
    // this.setState({
    //   profile: statP,
    //   operators: statO
    // });
    // var t1 = performance.now();
    // console.log("Call to api took " + (t1 - t0) + " milliseconds.");
  }

  handleChangePlatorm = (e) => {
    this.setState({
      platform: e.target.value
    });
  }

  handleChangeUsername = (e) => {
    this.setState({
      username: e.target.value
    });
  }

  handleSubmit = async(e) => {
    e.preventDefault();
    const statP =  await api.getStatProfile(this.state.username, this.state.platform);
    const statO =  await api.getStatOp(this.state.username, this.state.platform);
    this.setState({
      profile: statP,
      operators: statO
    });
  }



  render() {
  // animation bodymovin
  const bodymovinOptions = {
    loop: true,
    autoplay: true,
    prerender: true,
    animationData: animation
  }
    const {profile, operators} = this.state;
    if (!profile || !operators) {
      return (
        <div className="App">
          <form onSubmit={this.handleSubmit}>
            <input value={this.state.username} onChange={this.handleChangeUsername} type="text" name="username"/>
              <select value={this.state.platform} onChange={this.handleChangePlatorm}>
                <option value="pc">Pc</option>
                <option value="ps4">PS4</option>
                <option value="xbox">XBOX</option>
              </select>
            <button className="form__button">Search</button>
          </form>
          <div>
            <ReactBodymovin options={bodymovinOptions} />
          </div>
        </div>
      );
    }
    console.log(profile, operators);
    return (
      <div className="App">
        <form onSubmit={this.handleSubmit}>
          <input value={this.state.username} onChange={this.handleChangeUsername} type="text" name="username"/>
            <select value={this.state.platform} onChange={this.handleChangePlatorm}>
              <option value="pc">Pc</option>
              <option value="ps4">PS4</option>
              <option value="xbox">XBOX</option>
            </select>
          <button className="form__button">Search</button>
        </form>
      </div>
    );
  }
}

export default App;
